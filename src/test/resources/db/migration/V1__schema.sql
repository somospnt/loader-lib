CREATE TABLE persona (
    id                  BIGSERIAL PRIMARY KEY,
    nombre              VARCHAR NOT NULL,
    apellido            VARCHAR,
    altura              NUMERIC,
    peso                NUMERIC,
    fecha_nacimiento    DATE,
    esta_vivo           BOOLEAN,
    UNIQUE (nombre, apellido, fecha_nacimiento)
);