package com.somospnt.loaderlib.config;

import com.somospnt.loaderlib.controller.LoaderController;
import com.somospnt.loaderlib.service.LoaderService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoaderControllerAutoConfiguration {

    @Bean
    public LoaderController loaderController(LoaderService loaderService) {
        return new LoaderController(loaderService);
    }

}
