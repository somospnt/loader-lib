/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.loaderlib.config;

import com.somospnt.loaderlib.domain.Persona;
import com.somospnt.loaderlib.repository.PersonaRepository;
import com.somospnt.loaderlib.service.LoaderService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoaderServiceConfig {

    @Bean
    public LoaderService<Persona> loaderService(PersonaRepository repository) {
        return new LoaderService<>(repository, Persona.class,
                "nombre",
                "apellido",
                "altura",
                "peso",
                "fechaNacimiento",
                "estaVivo");
    }

}
