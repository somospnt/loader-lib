package com.somospnt.loaderlib.service;

import com.somospnt.loaderlib.ApplicationTests;
import com.somospnt.loaderlib.builder.PersonaBuilder;
import com.somospnt.loaderlib.builder.PersonaHostBuilder;
import com.somospnt.loaderlib.domain.Persona;
import com.somospnt.loaderlib.exception.LoaderException;
import java.io.ByteArrayInputStream;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.test.context.support.WithMockUser;

@WithMockUser(authorities = "EJECUTAR_LOADER")
public class LoaderServiceTypeIntegerTest extends ApplicationTests {

    @Autowired
    private LoaderService loaderService;

    @Autowired
    private JpaRepository<Persona, Long> repository;

    @Test
    public void cargar_lineaConCampoIntegerConCerosALaIzquierda_guardaSinCerosALaIzquierda() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conPeso("00000000056")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getPeso()).isEqualTo(56);
    }
    
    @Test
    public void cargar_lineaConCampoIntegerConDecimales_lanzaException() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conPeso("56.00")
                .buildLineaArchivo();

        assertThatThrownBy(() -> loaderService.cargar(new ByteArrayInputStream(entrada.getBytes())))
                .isInstanceOf(LoaderException.class)
                .hasMessageContaining("Error de formato. Linea [1] - Campo [peso] - Valor recibido [56.00]");
    }    
    
    @Test
    public void cargar_lineaConCampoBigDecimalVacio_guardaNull() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conPeso("")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getPeso()).isNull();
    }
    
    @Test
    public void cargar_lineaConCaracteresEnCampoTipoBigDecimal_lanzaExcepcion() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conPeso("PESO_INVALIDO")
                .buildLineaArchivo();

        assertThatThrownBy(() -> loaderService.cargar(new ByteArrayInputStream(entrada.getBytes())))
                .isInstanceOf(LoaderException.class)
                .hasMessageContaining("Error de formato. Linea [1] - Campo [peso] - Valor recibido [PESO_INVALIDO]");
    }
}
