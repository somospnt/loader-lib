package com.somospnt.loaderlib.service;

import com.somospnt.loaderlib.ApplicationTests;
import com.somospnt.loaderlib.builder.PersonaBuilder;
import com.somospnt.loaderlib.builder.PersonaHostBuilder;
import com.somospnt.loaderlib.domain.Persona;
import java.io.ByteArrayInputStream;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.test.context.support.WithMockUser;

@WithMockUser(authorities = "EJECUTAR_LOADER")
public class LoaderServiceTypeStringTest extends ApplicationTests {

    @Autowired
    private LoaderService loaderService;

    @Autowired
    private JpaRepository<Persona, Long> repository;

    @Test
    public void cargar_lineaConCampoTextoConEspaciosAIzquierdaYDerecha_guardaCampoSinEspacios() throws Exception {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conNombre("     Nombre con espacios       ")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getNombre()).isEqualTo("Nombre con espacios");
    }

    @Test
    public void cargar_lineaConCampoTextoConAcentos_guardaCampoConAcentos() throws Exception {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conNombre("María")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getNombre()).isEqualTo("María");
    }
    
    @Test
    public void cargar_lineaConCampoTextoVacio_cargaNull() throws Exception {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conApellido("")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getApellido()).isNull();
    }    
}
