package com.somospnt.loaderlib.service;

import com.somospnt.loaderlib.ApplicationTests;
import com.somospnt.loaderlib.builder.PersonaBuilder;
import com.somospnt.loaderlib.builder.PersonaHostBuilder;
import com.somospnt.loaderlib.domain.Persona;
import com.somospnt.loaderlib.exception.LoaderException;
import java.io.ByteArrayInputStream;
import java.time.LocalDate;
import java.time.Month;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.test.context.support.WithMockUser;

@WithMockUser(authorities = "EJECUTAR_LOADER")
public class LoaderServiceTypeLocalDateTest extends ApplicationTests {

    @Autowired
    private LoaderService loaderService;

    @Autowired
    private JpaRepository<Persona, Long> repository;

    @Test
    public void cargar_lineaConCampoTipoFechaConFormatoYYYYMMDD_cargaFecha() throws Exception {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conFechaNacimiento("19700215")
                .buildLineaArchivo();
        
        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getFechaNacimiento()).isEqualTo(LocalDate.of(1970, Month.FEBRUARY, 15));        
    }

    @Test
    public void cargar_lineaConCampoTipoFechaVacia_cargaNull() throws Exception {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conFechaNacimiento("")
                .buildLineaArchivo();
        
        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getFechaNacimiento()).isNull();        
    }

    @Test
    public void cargar_lineaConCaracteresEnCampoTipoFecha_lanzaExcepcion() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conFechaNacimiento("FECHA_INVALIDA")
                .buildLineaArchivo();

        assertThatThrownBy(() -> loaderService.cargar(new ByteArrayInputStream(entrada.getBytes())))
                .isInstanceOf(LoaderException.class)
                .hasMessageContaining("Error de formato. Linea [1] - Campo [fechaNacimiento] - Valor recibido [FECHA_INVALIDA]");
    }
}
