/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.loaderlib.service;

import com.somospnt.loaderlib.ApplicationTests;
import com.somospnt.loaderlib.builder.PersonaBuilder;
import com.somospnt.loaderlib.builder.PersonaHostBuilder;
import com.somospnt.loaderlib.domain.Persona;
import com.somospnt.loaderlib.exception.LoaderException;
import com.somospnt.loaderlib.repository.PersonaRepository;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;

@WithMockUser(authorities = "EJECUTAR_LOADER")
public class LoaderServiceTest extends ApplicationTests {

    @Autowired
    private LoaderService loaderService;
    @Autowired
    private LoaderService loaderServiceConCallbacks;

    @Autowired
    private PersonaRepository repository;

    @Test
    public void cargar_conLineaNuevaNoExistenteEnBD_insertaEnBD() throws Exception {
        Persona personaNueva = PersonaBuilder.valida().build();
        String entrada = PersonaHostBuilder.dePersona(personaNueva).buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        assertThat(repository.findAll())
                .usingElementComparatorOnFields("nombre", "apellido", "fechaNacimiento")
                .containsExactlyInAnyOrder(personaNueva);
    }

    @Test
    public void cargar_conLoaderServiceConCallbacks_ejecutaCallbacks() throws Exception {
        Persona personaNueva = PersonaBuilder.valida().build();
        String entrada = PersonaHostBuilder.dePersona(personaNueva).buildLineaArchivo();
        AtomicBoolean callbackBeforeLlamado = new AtomicBoolean();
        List<Persona> personas = new ArrayList<>();

        Runnable callbackBefore = () -> {
            callbackBeforeLlamado.set(true);
        };

        Writer<Persona> callbackSave = (persona) -> {
            personas.add(persona);
        };

        new LoaderService(
                callbackBefore,
                callbackSave,
                Persona.class,
                "nombre",
                "apellido",
                "altura",
                "peso",
                "fechaNacimiento",
                "estaVivo").cargar(new ByteArrayInputStream(entrada.getBytes()));

        assertThat(personas)
                .usingElementComparatorOnFields("nombre", "apellido", "fechaNacimiento")
                .containsExactlyInAnyOrder(personaNueva);
        assertThat(callbackBeforeLlamado).isTrue();
    }

    @Test
    public void cargar_variasLineasYNoExistenteEnBD_insertaEnBD() throws Exception {
        Persona personaNueva1 = PersonaBuilder.valida().build();
        Persona personaNueva2 = PersonaBuilder.valida().build();
        String entrada = PersonaHostBuilder.dePersona(personaNueva1).buildLineaArchivo() + "\n"
                + PersonaHostBuilder.dePersona(personaNueva2).buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        assertThat(repository.findAll())
                .usingElementComparatorOnFields("nombre", "apellido", "fechaNacimiento")
                .containsExactlyInAnyOrder(personaNueva1, personaNueva2);
    }

    @Test
    public void cargar_lineaExistenteEnBD_actualizaEnBD() throws Exception {
        PersonaBuilder.valida().build(entityManager);
        Persona personaNueva = PersonaBuilder.valida().conNombre("Nuevo Nombre").build();
        String entrada = PersonaHostBuilder.dePersona(personaNueva).buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        assertThat(repository.findAll())
                .usingElementComparatorOnFields("nombre", "apellido", "fechaNacimiento")
                .containsExactlyInAnyOrder(personaNueva);
    }

    @Test
    public void cargar_conSegundaLineaConError_lanzaExcepcionIndicandoNumeroDeLinea() {
        String entrada
                = PersonaHostBuilder
                        .dePersona(PersonaBuilder.valida().build())
                        .buildLineaArchivo() + "\n"
                + PersonaHostBuilder
                        .dePersona(PersonaBuilder.valida().build())
                        .conAltura("ALTURA_INVALIDA")
                        .buildLineaArchivo();

        assertThatThrownBy(() -> loaderService.cargar(new ByteArrayInputStream(entrada.getBytes())))
                .isInstanceOf(LoaderException.class)
                .hasMessageContaining("Error de formato. Linea [2] - Campo [altura] - Valor recibido [ALTURA_INVALIDA]");
    }

    @Test
    public void cargar_conMasCamposDeLosCorrespondientes_lanzaExcepcion() {
        String entrada
                = PersonaHostBuilder
                        .dePersona(PersonaBuilder.valida().build())
                        .buildLineaArchivo() + "|\n"
                + PersonaHostBuilder
                        .dePersona(PersonaBuilder.valida().build())
                        .buildLineaArchivo();

        assertThatThrownBy(() -> loaderService.cargar(new ByteArrayInputStream(entrada.getBytes())))
                .isInstanceOf(LoaderException.class)
                .hasMessageContaining("Error de formato. Linea [1] - Cantidad de campos invalida: esperada [6], recibida [7]");
    }

    @Test
    public void cargar_conCampoRequeridoVacio_lanzaExcepcion() {
        String entrada
                = PersonaHostBuilder
                        .dePersona(PersonaBuilder.valida().build())
                        .conNombre("")
                        .buildLineaArchivo();

        assertThatThrownBy(() -> loaderService.cargar(new ByteArrayInputStream(entrada.getBytes())))
                .isInstanceOf(LoaderException.class)
                .hasMessageContaining("Error de base de datos. Linea [1] - Se violo la restriccion ");
    }

    @Test
    public void cargar_conFilasConClavesDuplicada_lanzaExcepcion() {
        Persona persona1 = PersonaBuilder.valida().build();
        Persona persona2 = PersonaBuilder
                .conClave(persona1.getNombre(), persona1.getApellido(), persona1.getFechaNacimiento())
                .build();
        String entrada
                = PersonaHostBuilder.dePersona(persona1).buildLineaArchivo() + "\n"
                + PersonaHostBuilder.dePersona(persona2).buildLineaArchivo();

        assertThatThrownBy(() -> loaderService.cargar(new ByteArrayInputStream(entrada.getBytes())))
                .isInstanceOf(LoaderException.class)
                .hasMessageContaining("Error de base de datos. Linea [2] - Se violo la restriccion ");
    }

    @Test
    @WithMockUser(authorities = {})
    public void cargar_conUsuarioNoAutorizado_lanzaExcepcion() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .buildLineaArchivo();

        assertThatThrownBy(() -> loaderService.cargar((new ByteArrayInputStream(entrada.getBytes()))))
                .isInstanceOf(AccessDeniedException.class);
    }

    @Test
    public void cargar_conLoaderServiceMalConfigurado_lanzaExcepcion() {
        assertThatThrownBy(() -> new LoaderService<>(repository, Persona.class,
                "nombre",
                "apellidoo",
                "altura",
                "peso",
                "fechaNacimiento"))
                .isInstanceOf(IllegalArgumentException.class);
    }

}
