package com.somospnt.loaderlib.service;

import com.somospnt.loaderlib.ApplicationTests;
import com.somospnt.loaderlib.builder.PersonaBuilder;
import com.somospnt.loaderlib.builder.PersonaHostBuilder;
import com.somospnt.loaderlib.domain.Persona;
import com.somospnt.loaderlib.exception.LoaderException;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.test.context.support.WithMockUser;

@WithMockUser(authorities = "EJECUTAR_LOADER")
public class LoaderServiceTypeBigDecimalTest extends ApplicationTests {

    @Autowired
    private LoaderService loaderService;

    @Autowired
    private JpaRepository<Persona, Long> repository;

    @Test
    public void cargar_lineaConCampoBigDecimalConCerosALaIzquierda_guardaSinCerosALaIzquierda() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conAltura("0000000010.00")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getAltura()).isEqualTo(new BigDecimal("10.00"));
    }
    
    @Test
    public void cargar_lineaConCampoBigDecimalSinNumerosALaIzquierdaDelPunto_guardaDecimal() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conAltura(".33")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getAltura()).isEqualTo(new BigDecimal("0.33"));
    }   
    
    @Test
    public void cargar_lineaConCampoBigDecimalConSignoPositivo_guardaDecimal() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conAltura("+000100.00")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getAltura()).isEqualTo(new BigDecimal("100.00"));
    }    
    
    @Test
    public void cargar_lineaConCampoBigDecimalConSignoNegativo_guardaDecimal() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conAltura("-000100.00")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getAltura()).isEqualTo(new BigDecimal("-100.00"));
    }    

    @Test
    public void cargar_lineaConCampoBigDecimalSinNumerosALaIzquierdaDelPuntoYConSigno_guardaDecimal() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conAltura("-.33")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getAltura()).isEqualTo(new BigDecimal("-0.33"));
    }   
    
    @Test
    public void cargar_lineaConCampoBigDecimalVacio_guardaNull() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conAltura("")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getAltura()).isNull();
    }
    
    @Test
    public void cargar_lineaConCaracteresEnCampoTipoBigDecimal_lanzaExcepcion() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conAltura("ALTURA_INVALIDA")
                .buildLineaArchivo();

        assertThatThrownBy(() -> loaderService.cargar(new ByteArrayInputStream(entrada.getBytes())))
                .isInstanceOf(LoaderException.class)
                .hasMessageContaining("Error de formato. Linea [1] - Campo [altura] - Valor recibido [ALTURA_INVALIDA]");
    }
}
