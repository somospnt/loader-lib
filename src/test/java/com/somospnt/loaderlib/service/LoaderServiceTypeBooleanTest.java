package com.somospnt.loaderlib.service;

import com.somospnt.loaderlib.ApplicationTests;
import com.somospnt.loaderlib.builder.PersonaBuilder;
import com.somospnt.loaderlib.builder.PersonaHostBuilder;
import com.somospnt.loaderlib.domain.Persona;
import com.somospnt.loaderlib.exception.LoaderException;
import java.io.ByteArrayInputStream;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.test.context.support.WithMockUser;

@WithMockUser(authorities = "EJECUTAR_LOADER")
public class LoaderServiceTypeBooleanTest extends ApplicationTests {

    @Autowired
    private LoaderService loaderService;

    @Autowired
    private JpaRepository<Persona, Long> repository;

    @Test
    public void cargar_lineaConCampoBooleanTrueYesUno_cargaTrue() {
        String entrada = 
                PersonaHostBuilder.dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("true").buildLineaArchivo() + "\n"
              +  PersonaHostBuilder.dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("TRUE").buildLineaArchivo() + "\n"
              +  PersonaHostBuilder.dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("yes").buildLineaArchivo() + "\n"
              +  PersonaHostBuilder.dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("YES").buildLineaArchivo() + "\n"
              +  PersonaHostBuilder.dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("1").buildLineaArchivo() + "\n";
        

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        repository.findAll()
                .forEach((persona) -> assertThat(persona.getEstaVivo()).isTrue());
    }
    
    @Test
    public void cargar_lineaConCampoBooleanFalseNoCero_cargaFalse() {
        String entrada = 
                PersonaHostBuilder.dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("false").buildLineaArchivo() + "\n"
              +  PersonaHostBuilder.dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("FALSE").buildLineaArchivo() + "\n"
              +  PersonaHostBuilder.dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("no").buildLineaArchivo() + "\n"
              +  PersonaHostBuilder.dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("NO").buildLineaArchivo() + "\n"
              +  PersonaHostBuilder.dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("0").buildLineaArchivo() + "\n";
        
        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        repository.findAll()
                .forEach((persona) -> assertThat(persona.getEstaVivo()).isFalse());
    }    

    @Test
    public void cargar_lineaConCampoBooleanVacio_cargaNull() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("")
                .buildLineaArchivo();

        loaderService.cargar(new ByteArrayInputStream(entrada.getBytes()));

        Persona personaGuardada = repository.findAll().get(0);
        assertThat(personaGuardada.getEstaVivo()).isNull();
    }
    
    @Test
    public void cargar_lineaConCampoBooleanConOtrosCaracteres_lanzaExcepcion() {
        String entrada = PersonaHostBuilder
                .dePersona(PersonaBuilder.valida().build())
                .conEstaVivo("SI")
                .buildLineaArchivo();

        assertThatThrownBy(() -> loaderService.cargar(new ByteArrayInputStream(entrada.getBytes())))
                .isInstanceOf(LoaderException.class)
                .hasMessageContaining("Error de formato. Linea [1] - Campo [estaVivo] - Valor recibido [SI]");
    }
}
