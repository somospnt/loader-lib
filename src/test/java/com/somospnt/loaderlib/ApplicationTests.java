/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.loaderlib;

import com.somospnt.loaderlib.config.AuthorizationServerConfig;
import com.somospnt.loaderlib.config.LoaderServiceConfig;
import com.somospnt.loaderlib.config.OAuthHelperConfig;
import com.somospnt.loaderlib.config.ResourceServerConfig;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Import({
    LoaderServiceConfig.class,
    ResourceServerConfig.class,
    OAuthHelperConfig.class,
    AuthorizationServerConfig.class
})
public class ApplicationTests {

    @Autowired
    protected EntityManager entityManager;

    @Test
    public void contextLoads() {
    }

}
