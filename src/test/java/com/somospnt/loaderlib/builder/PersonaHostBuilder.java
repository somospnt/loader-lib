/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.loaderlib.builder;

import com.somospnt.loaderlib.builder.PersonaHostBuilder.PersonaHost;
import com.somospnt.loaderlib.domain.Persona;
import com.somospnt.test.builder.AbstractBuilder;
import java.time.format.DateTimeFormatter;

public class PersonaHostBuilder extends AbstractBuilder<PersonaHost> {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    private PersonaHostBuilder(Persona persona) {
        instance = new PersonaHost();
        instance.setNombre(persona.getNombre());
        instance.setApellido(persona.getApellido());
        instance.setAltura(persona.getAltura().toString());
        instance.setPeso(persona.getPeso().toString());
        instance.setFechaNacimiento(persona.getFechaNacimiento().format(formatter));
        instance.setEstaVivo(persona.getEstaVivo().toString());
    }

    public static PersonaHostBuilder dePersona(Persona persona) {
        PersonaHostBuilder builder = new PersonaHostBuilder(persona);
        return builder;
    }

    public PersonaHostBuilder conNombre(String nombre) {
        instance.setNombre(nombre);
        return this;
    }
    
    public PersonaHostBuilder conApellido(String apellido) {
        instance.setApellido(apellido);
        return this;
    }    

    public PersonaHostBuilder conFechaNacimiento(String fechaNacimiento) {
        instance.setFechaNacimiento(fechaNacimiento);
        return this;
    }

    public PersonaHostBuilder conAltura(String altura) {
        instance.setAltura(altura);
        return this;
    }

    public PersonaHostBuilder conPeso(String peso) {
        instance.setPeso(peso);
        return this;
    }
    
    public PersonaHostBuilder conEstaVivo(String estaVivo) {
        instance.setEstaVivo(estaVivo);
        return this;
    }

    public String buildLineaArchivo() {
        String linea = String.format("%s|%s|%s|%s|%s|%s",
                instance.getNombre(),
                instance.getApellido(),
                instance.getAltura(),
                instance.getPeso(),
                instance.getFechaNacimiento(),
                instance.getEstaVivo()
        );
        linea = linea.replace("null", "");
        return linea.replace(",", ".");
    }

    public static class PersonaHost {

        private String nombre;
        private String apellido;
        private String altura;
        private String peso;
        private String fechaNacimiento;
        private String estaVivo;

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getApellido() {
            return apellido;
        }

        public void setApellido(String apellido) {
            this.apellido = apellido;
        }

        public String getAltura() {
            return altura;
        }

        public void setAltura(String altura) {
            this.altura = altura;
        }

        public String getPeso() {
            return peso;
        }

        public void setPeso(String peso) {
            this.peso = peso;
        }

        public String getFechaNacimiento() {
            return fechaNacimiento;
        }

        public void setFechaNacimiento(String fechaNacimiento) {
            this.fechaNacimiento = fechaNacimiento;
        }

        public String getEstaVivo() {
            return estaVivo;
        }

        public void setEstaVivo(String estaVivo) {
            this.estaVivo = estaVivo;
        }
    }
}
