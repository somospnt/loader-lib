/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.loaderlib.builder;

import com.somospnt.loaderlib.domain.Persona;
import com.somospnt.test.builder.AbstractPersistenceBuilder;
import java.math.BigDecimal;
import java.time.LocalDate;

public class PersonaBuilder extends AbstractPersistenceBuilder<Persona> {

    private static int vecesInstanciado = 0;

    private PersonaBuilder() {
        instance = new Persona();
        instance.setNombre("Juan");
        instance.setApellido("Perez");
        instance.setAltura(new BigDecimal("1.70"));
        instance.setPeso(57);
        instance.setFechaNacimiento(LocalDate.now().minusYears(25).plusDays(vecesInstanciado));
        instance.setEstaVivo(true);
        vecesInstanciado++;
    }

    public static PersonaBuilder valida() {
        PersonaBuilder builder = new PersonaBuilder();
        return builder;
    }

    public static PersonaBuilder conClave(String nombre, String apellido, LocalDate fechaNacimiento) {
        PersonaBuilder builder = new PersonaBuilder();
        builder.instance.setNombre(nombre);
        builder.instance.setApellido(apellido);
        builder.instance.setFechaNacimiento(fechaNacimiento);
        return builder;
    }
    
    public PersonaBuilder conNombre(String nombre) {
        instance.setNombre(nombre);
        return this;
    }    
}
