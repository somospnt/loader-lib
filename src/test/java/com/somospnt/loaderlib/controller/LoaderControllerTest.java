/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.loaderlib.controller;

import com.somospnt.loaderlib.ApplicationTests;
import com.somospnt.loaderlib.builder.PersonaBuilder;
import com.somospnt.loaderlib.builder.PersonaHostBuilder;
import com.somospnt.loaderlib.domain.Persona;
import com.somospnt.loaderlib.security.OAuthHelper;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class LoaderControllerTest extends ApplicationTests {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected OAuthHelper oAuth2Helper;

    @Autowired
    private JpaRepository<Persona, Long> repository;

    @Test
    public void cargar_archivoValido_guardaRegistrosEnBD() throws Exception {
        String contenidoArchivo
                = PersonaHostBuilder.dePersona(PersonaBuilder.valida().build()).buildLineaArchivo() + "\n"
                + PersonaHostBuilder.dePersona(PersonaBuilder.valida().build()).buildLineaArchivo() + "\n";
        MockMultipartFile file = new MockMultipartFile("archivo", "archivo.txt",
                "text/plain", contenidoArchivo.getBytes());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/loader")
                .file(file)
                .with(oAuth2Helper.bearerToken("rundeck")))
                .andExpect(status().is(200));
        assertThat(repository.findAll()).hasSize(2);
    }

    //@Test
    public void cargar_archivoConError_devuelve400() throws Exception {
        String contenidoArchivo
                = PersonaHostBuilder.dePersona(PersonaBuilder.valida().build()).buildLineaArchivo() + "\n"
                + PersonaHostBuilder.dePersona(PersonaBuilder.valida().build()).conFechaNacimiento("FECHA_INVALIDA").buildLineaArchivo() + "\n";
        MockMultipartFile file = new MockMultipartFile("archivo", "archivo.txt",
                "text/plain", contenidoArchivo.getBytes());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/loader")
                .file(file)
                .with(oAuth2Helper.bearerToken("rundeck")))
                .andExpect(status().is(400));
    }

    //@Test
    public void cargar_archivoConNombreVariableInvalido_retorna400() throws Exception {
        String contenidoArchivo
                = PersonaHostBuilder.dePersona(PersonaBuilder.valida().build()).buildLineaArchivo();
        MockMultipartFile file = new MockMultipartFile("X", "archivo.txt",
                "text/plain", contenidoArchivo.getBytes());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/loader")
                .file(file)
                .with(oAuth2Helper.bearerToken("rundeck")))
                .andExpect(status().is(400))
                .andExpect(status().reason(containsString("Se debe pasar como argumento un archivo con nombre de variable 'archivo'")));
    }

    //@Test
    public void cargar_sinArchivo_devuelve400() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.multipart("/loader")
                .with(oAuth2Helper.bearerToken("rundeck")))
                .andExpect(status().is(400))
                .andExpect(status().reason(containsString("Se debe pasar como argumento un archivo con nombre de variable 'archivo'")));
    }
}
