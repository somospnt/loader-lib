/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.loaderlib.exception;

import org.springframework.batch.item.file.transform.IncorrectTokenCountException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

public class LoaderException extends RuntimeException {

    private static final String MENSAJE_BIND = "Error de formato. Linea [%s] - Campo [%s] - Valor recibido [%s]";
    private static final String MENSAJE_LENGTH = "Error de formato. Linea [%s] - Cantidad de campos invalida: esperada [%s], recibida [%s]";
    private static final String MENSAJE_CONSTRAINT = "Error de base de datos. Linea [%s] - Se violo la restriccion [%s]";

    public LoaderException(Exception exception, int numeroLinea) {
        super(getMessage(exception, numeroLinea), exception);
    }

    private static String getMessage(Exception exception, int numeroLinea) {
        if (exception instanceof BindException) {
            BindException bindException = (BindException) exception;
            String campo = bindException.getFieldError().getField();
            return String.format(MENSAJE_BIND,
                    numeroLinea,
                    campo,
                    bindException.getFieldValue(campo));
        } else if (exception instanceof IncorrectTokenCountException) {
            IncorrectTokenCountException incorrectTokenCountException = (IncorrectTokenCountException) exception;
            return String.format(MENSAJE_LENGTH,
                    numeroLinea,
                    incorrectTokenCountException.getExpectedCount(),
                    incorrectTokenCountException.getActualCount());
        } else if (exception instanceof DataIntegrityViolationException) {
            DataIntegrityViolationException dataIntegrityViolationException = (DataIntegrityViolationException) exception;
            return String.format(MENSAJE_CONSTRAINT,
                    numeroLinea,
                    dataIntegrityViolationException.getRootCause().getMessage());
        }
        return exception.getMessage();
    }
}
