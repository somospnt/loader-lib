package com.somospnt.loaderlib.service;

@FunctionalInterface
public interface Writer<T> {

    void write(T obj);
}
