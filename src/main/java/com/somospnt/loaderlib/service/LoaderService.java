/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.loaderlib.service;

import com.somospnt.loaderlib.exception.LoaderException;
import com.somospnt.loaderlib.factory.LineMapperFactory;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class LoaderService<T> {

    private final LineMapper<T> lineMapper;
    private final Writer<T> callbackSave;
    private static final Logger LOG = LoggerFactory.getLogger(LoaderService.class);
    private final Runnable callbackBefore;

    public LoaderService(
            JpaRepository<T, Long> jpaRepository,
            Class<T> clase,
            String... nombresCampos) {
        this.callbackBefore = () -> jpaRepository.deleteAllInBatch();
        this.callbackSave = obj -> jpaRepository.save(obj);
        this.lineMapper = LineMapperFactory.crear(clase, nombresCampos);
        this.charsetEncoding = Charset.forName("windows-1252");
    }

    public LoaderService(
            Runnable callbackBefore,
            Writer<T> callbackSave,
            Class<T> clase,
            String... nombresCampos) {
        this.lineMapper = LineMapperFactory.crear(clase, nombresCampos);
        this.callbackBefore = callbackBefore;
        this.callbackSave = callbackSave;
        this.charsetEncoding = Charset.forName("windows-1252");
    }

    @Value("${loader.charset.encoding:windows-1252}")
    private Charset charsetEncoding;

    /**
     * Carga un inputStream en una tabla.
     *
     */
    @PreAuthorize(value = "hasAuthority('EJECUTAR_LOADER')")
    public void cargar(InputStream inputStream) {
        LOG.info("*********** COMIENZO LOADER ******************");
        ejecutarCallbackBefore();
        cargarInputStream(inputStream);
        LOG.info("*********** FIN LOADER ***********************\n");
    }

    private void ejecutarCallbackBefore() {
        callbackBefore.run();
    }

    private void cargarInputStream(InputStream inputStream) {
        LineNumberReader reader = new LineNumberReader(new InputStreamReader(inputStream, charsetEncoding));
        reader.lines().forEach(linea -> {
            this.cargarLinea(linea, reader.getLineNumber());
        });
        LOG.info(" Cantidad de lineas insertadas: {}", reader.getLineNumber());
    }

    private void cargarLinea(String linea, int numeroLinea) {
        try {
            callbackSave.write(lineMapper.mapLine(linea, 0));
        } catch (Exception ex) {
            throw new LoaderException(ex, numeroLinea);
        }
    }

}
