/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.loaderlib.controller;

import com.somospnt.loaderlib.exception.LoaderException;
import com.somospnt.loaderlib.service.LoaderService;
import java.io.IOException;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

@RestController
public class LoaderController {

    private final LoaderService loaderService;
    private static final Logger LOG = LoggerFactory.getLogger(LoaderController.class);

    public LoaderController(LoaderService loaderService) {
        this.loaderService = loaderService;
    }

    @PostMapping("/loader")
    public void cargar(@RequestParam(value = "archivo", required = true) MultipartFile archivo) throws Exception {
        loaderService.cargar(archivo.getInputStream());
    }

    @ExceptionHandler(LoaderException.class)
    public void handleLoader(Exception exception, HttpServletResponse response) throws IOException {
        LOG.error(exception.getMessage());
        response.sendError(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
    }

    @ExceptionHandler({MultipartException.class, MissingServletRequestPartException.class})
    public void handleArchivo(Exception exception, HttpServletResponse response) throws IOException {
        String mensaje = "Se debe pasar como argumento un archivo con nombre de variable 'archivo'";
        LOG.error(mensaje);
        response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
    }
}
