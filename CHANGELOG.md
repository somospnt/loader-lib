# Changelog
Todos los cambios relevantes a este proyecto serán documentados en este archivo.

El formato esta basado en [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
y el proyecto adhiere a [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [2.0.0] 21-09-2023

## Changed

- Se cambia javax servlet por jakarta servlet
- Se sube la versión de java a 11
- Se deja del lado del consumidor de la librería la configuración de los beans desde el controller.

## [1.0.3] 28-12-2018

### Added

- Se agrega un callbackBefore a la definicion del Bean de LoaderService para hacer flexible la tarea a ejecutar previa a ejecutar el loader. Por default, si no se define el callback se vacia la tabla completa.

### Removed

- Se elimino @EnableGlobalMethodSecurity(prePostEnabled = true) de la libreria y la habilitacion de la seguridad la debe tener la app que utiliza la libreria

### Changed

- Se bajo la version de Java de 11 a 8 para no obligar a las app que usan la libreria a tener que usar java 11.

## [1.0.2] 21-12-2018

### Added

- Se agrega @EnableGlobalMethodSecurity(prePostEnabled = true) para habilitar la seguridad

## [1.0.1] 20-12-2018

### Fixed

- Si una campo texto viene en blanco en el archivo, se carga NULL en la tabla 

## [1.0.0] 18-12-2018

### Added

- Version inicial de loader-lib