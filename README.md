# loader-lib

Libreria para hacer loader de archivo a tabla por HTTP


## Como armar el archivo

* Los campos deben estar separados por "|"
* No se debe poner un "|" al final de la linea
* Los campos de tipo fecha deben tener el formato yyyyMMdd
* Los campos de tipo decimal deben venir con el separador decimal de "."
* Los nulos se representan como dos "|" seguidos: "||"

## Como invocar el servicio

    curl -X POST -H "Authorization: Bearer {{token}}" -F "archivo=@{{path archivo}}" http://{{host:port}}/loader

## Como funciona

* Los espacios al final y al principio de un campo de tipo texto no se guardan
* Los ceros a la izquierda en un campo de tipo entero o decimal no se guardan
* Para cargar un booleano se puede usar true|false o 1|0
* Se borra la tabla y se carga completa cada vez (no hay novedades)
* Si el loader es exitoso se retorna Status HTTP 200
* Si falla el loader en una linea:
   - Se retorna status HTTP 400
   - Se retorna mensaje de error en el body donde se informa el motivo y la linea donde sucedio el error
   - La tabla queda como estaba antes del loader
* Por default se utiliza el charset WIN-1252 


## Seguridad OAUTH

* Si el proyecto que usa la libreria tiene seguridad OAUTH en la tabla de OAUTH tiene que existir un cliente con el authorities EJECUTAR_LOADER

## Como usarla en un proyecto

para poder utilizar esta librería realizar la siguiente configuracion:


    @Bean
    public LoaderService<Persona> loaderService(PersonaRepository repository) {
        return new LoaderService<>(repository, Persona.class,
                "nombre",
                "apellido",
                "altura",
                "peso",
                "fechaNacimiento");
    }

    @Bean
    public LoaderController loaderController(LoaderService loaderService) {
        return new LoaderController(loaderService);
    }

Donde
- repository es el JpaRepository que se utiliza para hacer el save
- Persona es el objeto de dominio
- y se definen los campos que se cargan desde el archivo en el orden que aparecen en el archivo

* Por property se puede sobrescribir el charset a utilizar, por default es WIN-1252:
   - loader.charset.encoding

Si se desea ejecutar un callback antes de cargar el archivo se puede setear el callback al bean:

    loaderService.setCallbackBefore(() -> {
            repository.deleteByNombreIn(nombres);
        });

Por default se ejecuta el callback que borra toda la tabla: 

    repository.deleteAllInBatch();


Esta librería esta bajo la [licencia](LICENSE).
